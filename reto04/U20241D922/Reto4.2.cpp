#include <iostream>
#include <ctime>

using namespace std;

int** crear_matriz(int n, int m);
void imprimir_matriz(int** matriz, int n, int m);
void limpiar_mariz(int** matriz, int n, int m);

int main() {
	int n, m;

	cout << "Ingrese las filas del arreglo: "; cin >> n;
	cout << "Ingrese las columnas del arreglo: "; cin >> m;

	int **matriz = crear_matriz(n, m);
	imprimir_matriz(matriz, n, m);
	
	for (int i = 0; i < n; ++i) {
		delete[] matriz[i];
	}
	delete[] matriz;

	system("pause");
	return 0;
}
int** crear_matriz(int n, int m) {
	int** matriz = new int*[n];

	for (int i = 0; i < n; ++i) {
		matriz[i] = 0;
		matriz[i] = new int[m];
		for (int j = 0; j < m; ++j) {
			matriz[i][j] = 0;
		}
	} 
	return matriz;
}
void imprimir_matriz(int** matriz,  int n, int m) {
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			cout << matriz[i][j] << " ";
		}
		cout << endl;
	}
}

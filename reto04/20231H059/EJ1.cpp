#include <iostream>
#include <ctime>

using namespace std;

int* CrearArreglo(int tamanomin, int tamanomax, int& tamanofinal);

int main() {
			
	srand(time(nullptr));

	int tamanofinal;
	CrearArreglo(100, 500, tamanofinal);
	
	//Imprimiendo el tamano del arreglo y sus elementos para comprobar
	cout << "Tamano del arreglo: " << tamanofinal << endl;
	for (int i = 0; i < tamanofinal; ++i) {
		cout << arreglo[i] << endl;
	}

	delete[]arreglo;
	system("pause");
	return EXIT_SUCCESS;
}

int* CrearArreglo(int tamanomin, int tamanomax, int& tamanofinal) {

	//Tamano aleatorio entre 100 y 500
	tamanofinal = tamanomin + rand() % (tamanomax - tamanomin);
	int *arreglo = new int[tamanofinal];	

	for (int i = 0; i < tamanofinal; ++i) {
		arreglo[i] = (rand() % 10000) + 1;
	}

	return arreglo;

}